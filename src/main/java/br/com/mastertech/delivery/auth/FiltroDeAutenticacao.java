package br.com.mastertech.delivery.auth;

import br.com.mastertech.delivery.DTOs.LoginClienteDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class FiltroDeAutenticacao extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private JWTUtil jwtUtil;

    public FiltroDeAutenticacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        ObjectMapper objectMapper = new ObjectMapper();

        try{
            LoginClienteDTO loginClienteDTO = objectMapper.readValue(request.getInputStream(), LoginClienteDTO.class);


            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    loginClienteDTO.getCpf(), loginClienteDTO.getSenha(), new ArrayList<>()
            );
            Authentication authentication = authenticationManager.authenticate(authToken);
            return authentication;
        }catch(IOException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        String cpf = ((AuthCliente) authResult.getPrincipal()).getUsername();
        String token = jwtUtil.gerarToken(cpf);

        response.addHeader("Authorization", "Bearer "+token);;
    }
}
