package br.com.mastertech.delivery.repositories;

import br.com.mastertech.delivery.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    Cliente findByCpf(String cpf);
}

