package br.com.mastertech.delivery.services;

import br.com.mastertech.delivery.auth.AuthCliente;
import br.com.mastertech.delivery.models.Cliente;
import br.com.mastertech.delivery.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ClienteService implements UserDetailsService {


    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Cliente salvarCliente(Cliente cliente){
        String senha = cliente.getSenha();
        cliente.setSenha(encoder.encode(senha));
        return clienteRepository.save(cliente);
    }

    @Override
    public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
        Cliente cliente = clienteRepository.findByCpf(cpf);
        if (cliente == null){
            throw new UsernameNotFoundException("Cliente não cadastrado");
        }
        AuthCliente authCliente = new AuthCliente(cliente.getId(), cliente.getCpf(), cliente.getSenha());
        return  authCliente;
    }
}
